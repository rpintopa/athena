/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_GENPARTICLEFWD_H
#define ATLASHEPMC_GENPARTICLEFWD_H
namespace HepMC {
class GenParticle;
typedef GenParticle* GenParticlePtr;
typedef (GenParticle* const) ConstGenParticlePtr;
}
#endif
